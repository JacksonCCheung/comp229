package comp229;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MouseInfo;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Stage extends JPanel {

	static JPanel panel = new JPanel();
	int nR = 20;
	int nC = 20;
	
	grid grids = new grid(nR,nC);
	Sheep s = new Sheep();
	Wolf w = new Wolf();
	Shepherd p = new Shepherd();
	
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		
		g2.setColor(Color.green);
		
		for (int i = 0; i < nR; i++){
			for ( int x = 0; x< nC; x++){
			
			g2.fillRect(grids.cells[i][x].getX()*i, grids.cells[i][x].getY()*x, 
					35, 35);
			
			}
		}
		
		
			
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("Week3");
		frame.setSize(1280, 800);
		
		Stage p = new Stage();
		frame.add(p);
		
		frame.setVisible(true);
		
	}

}
