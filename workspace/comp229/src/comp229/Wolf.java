package comp229;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

public class Wolf implements Characters{

	@Override
	public void paint(Graphics2D g2, Point mouseLoc) {
		
		g2.setColor(Color.red);
		g2.fillRect(mouseLoc.x, mouseLoc.y, 35, 35);
		
	}

}
