package comp229;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
 

import javax.swing.JFrame;
import javax.swing.JPanel;
 
 
public class Cell extends JPanel {
 
	private int x,y;
	private int width,height;
	
	
	public Cell(){

		this.width = 35;
		this.height = 35;
		this.x = 0;
		this.y = 0;
		
	}
	
	public Cell(int width,int height){
		
		this.width = width;
		this.height = height;
		
	}
	
	public Cell(int width,int height,int x, int y){
		
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
		
	}
	
	public void setX(int set){
		
		this.x = set;
		
	}
	
	public void setY(int set){
		
		this.y = set;
		
	}

	public int getX(){
		
		return this.x;
		
	}
	
	public int getY(){
		
		return this.y;
		
	}
	
	public int getWidth(){
		
		return this.width;
		
	}
	
	public int getHeight(){
		
		return this.height;
		
	}

 
}