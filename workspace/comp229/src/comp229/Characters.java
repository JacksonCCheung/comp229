package comp229;

import java.awt.Graphics2D;
import java.awt.Point;

public interface Characters {
	
	public void paint(java.awt.Graphics2D g2, java.awt.Point mouseLoc);
	
}
