package comp229;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
 
 
public class grid {
 
	Cell cells[][];
	
	grid(int nRow,int nCol){
		
		cells = new Cell[nRow][nCol];
		for (int i = 0; i < nRow; i++){
			for ( int x = 0 ; x < nCol ;x++){
				
				Cell c = new Cell(i,x,35,35);
				cells[i][x] = c;
			}
		}
		
	}
	
	Cell getCell(int r, int c){
		
		return cells[r][c];
		
	}
	
	
	public static void main(String [] args) {
		
	}
 
}